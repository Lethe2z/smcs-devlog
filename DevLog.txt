06.12.2023
- Quick Start compiled and run.

12.12.2023 
- Internet dependency has been removed. Dependencies have been packaged and embedded within the project.
- SMCS repository has been created.

15.12.2023
- DevLog repository has been created.

06.01.2024
- Deploy script is added.